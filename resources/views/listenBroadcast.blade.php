<htmL>
    <head>
        <title>Listen Page</title>
        <link href="{{ asset('css/app.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
    <p>Your User Id: {{ $user_id }}</p>
    <p>Your Department Id: {{ $department_id }}</p>
    <p>Your User Type Id: {{ $user_type }}</p>

    <div id="app">
        <example-component :user_id="<?php echo $user_id; ?>"
        :department_id="<?php echo $department_id; ?>"
        :user_type="<?php echo $user_type; ?>"></example-component>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    </body>
</htmL>
