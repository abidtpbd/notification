<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\UserType;
use App\Message;
use Auth;
use App\Events\MessageNotification;

class NotificationController extends Controller
{
    public function launcher(){
        $departments = Department::all();
        $userTypes = UserType::all();

        return view('notificationLauncher',['departments'=>$departments,'userTypes'=>$userTypes]);
    }
    public function send(Request $request){
       //dd($request);
       $user_id = Auth::user()->id;
       $message = new Message();
       $message->from = $user_id;
       $message->message = $request['message'];
       if(isset($request['departmentType']) && $request['departmentType'] != ""){
        $message->to_department = $request['departmentType'];
        $message->to_user = null;
       }else{
         $message->to_user = $request['userType'];
         $message->to_department = null;
       }
       $mess_save = $message->save();

       if($mess_save){
        event(new MessageNotification($message));
       }else{
           dd('do not save');
       }


    }
}
