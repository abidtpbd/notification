@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Broadcasting Panel</div>



                <div class="card-body">
                    <form action="{{ url('send') }}" method="post">
                         {{csrf_field()}}

                        <div class="form-group">
                            <label>Broadcast Notification To:</label>
                            <select class="form-control" name="notifiTo" id="notifiTo">
                                <option value=""> ----- Select One ----- </option>
                                <option value="user">User Wise</option>
                                <option value="department">Department Wise</option>
                            </select>
                        </div>

                        <div class="form-group" id="departmentType" style="display: none;">
                            <label>Department Type:</label>
                            <select class="form-control" name="departmentType" id="departmentTypeValue">
                                <option value=""> ----- Select One ------ </option>

                                @foreach($departments as $department){
                                    <option value="{{ $department['id'] }}">{{ $department['name'] }}</option>
                                }
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group" id="userType" style="display: none;">
                            <label>User Type:</label>
                            <select class="form-control" name="userType" id="userTypeValue">
                                <option value=""> ----- Select One ------ </option>
                                    @foreach($userTypes as $userType)
                                        <option value="{{ $userType['id'] }}">{{ $userType['name'] }}</option>
                                    @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Message:</label>
                            <textarea class="form-control" name="message" rows="5" id="message"></textarea>
                        </div>

                        <div class="form-group">
                            <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('javascprit')


<script>
$(document).ready(function(){

    $("#notifiTo").on('change',function(){
        var notifiTo = $(this).find(":selected").val();
        if(notifiTo == 'user'){
            $('#userType').show();
            $('#departmentType').hide();
            $('#departmentTypeValue').val("");
        }else{
            $('#userType').hide();
            $('#userTypeValue').val("");
            $('#departmentType').show();
        }
    });
});
</script>
@endsection





