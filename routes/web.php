<?php

use App\Events\TaskEvent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// -------------- For Test ----------------------

Route::get('/event', function () {
    event (new TaskEvent('In The event router'));
});

Route::post('/user_id',function(){
    return '3';
});



Auth::routes();


// ----------- For Notification ----------------------------


Route::group(array('middleware'=>'auth'),function(){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/launcher',array(
        'uses'=>'NotificationController@launcher',
        'as'=>'launcher'
    ));

    Route::post('/send',array(
        'uses'=>'NotificationController@send',
        'as'=>'send'
    ));

    Route::get('/listen',function(){
        $user_id = Auth::user()->id;
        $department_id = Auth::user()->department;
        $user_type = Auth::user()->user_type;
        return view('listenBroadcast',['user_id'=>$user_id,'department_id'=>$department_id,'user_type'=>$user_type]);
    });

});
