<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            [
                'name' => 'Admin',
                'created_at' =>  Carbon::now(),
            ],
            [
                'name' => 'HR',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Accounts',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Sales',
                'created_at' => Carbon::now(),
            ]
        ]);
    }
}
