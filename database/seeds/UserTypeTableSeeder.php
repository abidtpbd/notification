<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->insert([
            [
                'name' => 'Users',
                'created_at' =>  Carbon::now(),
            ],
            [
                'name' => 'Customers',
                'created_at' => Carbon::now(),
            ],
            [
                'name' => 'Vendors',
                'created_at' => Carbon::now(),
            ]
        ]);
    }
}
